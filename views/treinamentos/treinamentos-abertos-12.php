    <div class="main treinamentos">
        <div class="title"></div>

        <div class="center">
            <div class="aside">
<?php
$sub = 'treinamentos-abertos';
include 'include/aside.php';
?>
            </div>

            <div class="conteudo">
                <img src="<?=$url?>assets/img/layout/img-treinamentos-abertos.png" alt="">

                <h3>
                    TREINAMENTOS ABERTOS
                    <span class="area">área de atuação: <strong>QUALIDADE</strong></span>
                </h3>

                <div class="treinamento-descricao no-margin">
                    <div class="titulo">Nome do treinamento completo</div>
                    <div class="treinamento-data no-hover">
                        <div class="data">23 <strong>JUNHO</strong> 2015</div>
                        <div class="endereco">
                            <span><strong>São Paulo - SP</strong> | Auditório do Instituto Falcão Bauer</span>
                            <span class="icone">Rua do Endereço Completo, 123 · Bairro da Vila · Cidade, UF</span>
                        </div>
                    </div>
                </div>

                <h4>FORMULÁRIO DE INSCRIÇÃO</h4>
                <div class="inscricao-wrapper">
                    <div class="passos">
                        <div class="passo">
                            <span>CRIAR LOGIN</span>
                            <span class="numero">1</span>
                        </div>
                        <div class="passo">
                            <span>INFORMAR<br>DADOS PESSOAIS</span>
                            <span class="numero">2</span>
                        </div>
                        <div class="passo active">
                            <span>INFORMAR<br>DADOS DE COBRANÇA</span>
                            <span class="numero">3</span>
                        </div>
                        <div class="passo">
                            <span>CONFIRMAR E<br>FINALIZAR INSCRIÇÃO</span>
                            <span class="numero">4</span>
                        </div>
                    </div>

                    <h5>NOVO CADASTRO</h5>
                    <h6>RESPONSÁVEL PELO PAGAMENTO: PARTICIPANTE</h6>
                    <h6>DADOS DE PAGAMENTO</h6>
                    <form action="" method="post" class="form-inscricao-padrao">
                        <label class="forma-pagamento">
                            <input type="radio" name="pagamento" value="boleto">
                            boleto
                        </label>
                        <label class="forma-pagamento">
                            <input type="radio" name="pagamento" value="cartao">
                            cartão de crédito
                        </label>
                        <div class="row">
                            <label>bandeira</label>
                            <div class="formulario">
                                <label>
                                    <input type="radio" name="bandeira" value="visa">
                                    VISA
                                </label>
                                <label>
                                    <input type="radio" name="bandeira" value="mastercard">
                                    Mastercard
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <label for="numero-cartao">número do cartão</label>
                            <div class="formulario">
                                <input type="text" name="numero-cartao" id="numero-cartao">
                            </div>
                        </div>
                        <div class="row">
                            <label for="vencimento-cartao">vencimento</label>
                            <div class="formulario">
                                <input type="text" name="vencimento-cartao" id="vencimento-cartao">
                            </div>
                        </div>
                        <div class="row">
                            <label for="codigo-seguranca-cartao">código de segurança</label>
                            <div class="formulario">
                                <input type="text" name="codigo-seguranca-cartao" id="codigo-seguranca-cartao">
                            </div>
                        </div>
                        <div class="row codigo-promocional">
                            <label for="codigo-promocional">CÓDIGO PROMOCIONAL</label>
                            <div class="formulario">
                                <input type="text" name="codigo-promocional" id="codigo-promocional">
                                <p>Se você possui um código promocional insira-o aqui para obter seu desconto.</p>
                            </div>
                        </div>
                        <a href="#" class="form-submit">
                            <span>PROSSEGUIR: PASSO 4: CONFIRMAR E FINALIZAR INSCRIÇÃO</span>
                        </a>
                    </form>
                </div>
            </div>
        </div>
    </div>
