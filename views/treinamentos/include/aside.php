                <h2>TREINAMENTOS</h2>
                <nav class="nav-treinamentos">
                    <a href="<?=$url?>treinamentos/nossa-abordagem"<?php if($sub == 'nossa-abordagem') echo 'class="active"' ?>>NOSSA ABORDAGEM</a>
                    <a href="<?=$url?>treinamentos/treinamentos-abertos"<?php if($sub == 'treinamentos-abertos') echo 'class="active"' ?>>TREINAMENTOS ABERTOS</a>
                    <a href="<?=$url?>treinamentos/calendario-programacao"<?php if($sub == 'calendario-programacao') echo 'class="active"' ?>>CALENDÁRIO · PROGRAMAÇÃO</a>
                    <a href="<?=$url?>treinamentos/in-company"<?php if($sub == 'in-company') echo 'class="active"' ?>>TREINAMENTOS IN COMPANY</a>
                    <a href="<?=$url?>treinamentos/instrutores"<?php if($sub == 'instrutores') echo 'class="active"' ?>>INSTRUTORES</a>
                    <a href="<?=$url?>treinamentos/portal-do-aluno"<?php if($sub == 'portal-do-aluno') echo 'class="active"' ?>>PORTAL DO ALUNO</a>
                </nav>

                <h3>ÁREAS DE ATUAÇÃO</h3>
                <nav class="nav-atuacao">
                    <a href="#">MEIO AMBIENTE</a>
                    <a href="#">NORMAS TÉCNICAS</a>
                    <a href="#">QUALIDADE</a>
                    <a href="#">RH</a>
                    <a href="#">SAÚDE E SEGURANÇA</a>
                    <a href="#">SSO</a>
                    <a href="#">SUSTENTABILIDADE</a>
                </nav>

                <a href="<?=$url?>treinamentos/interesse" class="interesse<?php if($sub == 'interesse') echo ' active' ?>">
                    <span>REGISTRE<br>SEU<br>INTERESSE</span>
                </a>
                <a href="<?=$url?>treinamentos/instrutores" class="instrutores<?php if($sub == 'instrutores') echo ' active' ?>">
                    <span>CONHEÇA<br>NOSSOS<br>INSTRUTORES</span>
                </a>
