    <div class="main treinamentos">
        <div class="title"></div>

        <div class="center">
            <div class="aside">
<?php
$sub = '';
include 'include/aside.php';
?>
            </div>

            <div class="conteudo">
                <img src="<?=$url?>assets/img/layout/img-treinamentos.png" alt="">

                <div class="proximos-treinamentos">
                    <div class="titulo">
                        <p>Próximos Treinamentos:</p>
                        <a href="#">CONSULTAR CALENDÁRIO COMPLETO »</a>
                    </div>

                    <a href="#" class="treinamento-chamada">
                        <div class="data">
                            <span class="dia">02</span>
                            <span class="mes">JUN</span>
                        </div>
                        <div class="titulo">
                            FORMAÇÃO DE AUDITOR INTERNO ISO 9001:2008
                            <span>Qualidade</span>
                        </div>
                        <div class="local">
                            São Paulo - SP
                        </div>
                    </a>
                    <a href="#" class="treinamento-chamada">
                        <div class="data">
                            <span class="dia">06</span>
                            <span class="mes">JUL</span>
                        </div>
                        <div class="titulo">
                            FORMAÇÃO DE AUDITOR INTERNO COM NOME MUITO LONGO PARA OCUPAR DUAS LINHAS ISO 9001:2008
                            <span>Saúde e Segurança</span>
                        </div>
                        <div class="local">
                            São Paulo - SP
                        </div>
                    </a>
                    <a href="#" class="treinamento-chamada">
                        <div class="data">
                            <span class="dia">02</span>
                            <span class="mes">JUN</span>
                        </div>
                        <div class="titulo">
                            FORMAÇÃO DE AUDITOR INTERNO ISO 9001:2008
                            <span>Qualidade</span>
                        </div>
                        <div class="local">
                            São Paulo - SP
                        </div>
                    </a>
                    <a href="#" class="treinamento-chamada">
                        <div class="data">
                            <span class="dia">06</span>
                            <span class="mes">JUL</span>
                        </div>
                        <div class="titulo">
                            FORMAÇÃO DE AUDITOR INTERNO COM NOME MUITO LONGO PARA OCUPAR DUAS LINHAS ISO 9001:2008
                            <span>Saúde e Segurança</span>
                        </div>
                        <div class="local">
                            São Paulo - SP
                        </div>
                    </a>
                    <a href="#" class="treinamento-chamada">
                        <div class="data">
                            <span class="dia">02</span>
                            <span class="mes">JUN</span>
                        </div>
                        <div class="titulo">
                            FORMAÇÃO DE AUDITOR INTERNO ISO 9001:2008
                            <span>Qualidade</span>
                        </div>
                        <div class="local">
                            São Paulo - SP
                        </div>
                    </a>
                    <a href="#" class="treinamento-chamada">
                        <div class="data">
                            <span class="dia">06</span>
                            <span class="mes">JUL</span>
                        </div>
                        <div class="titulo">
                            FORMAÇÃO DE AUDITOR INTERNO COM NOME MUITO LONGO PARA OCUPAR DUAS LINHAS ISO 9001:2008
                            <span>Saúde e Segurança</span>
                        </div>
                        <div class="local">
                            São Paulo - SP
                        </div>
                    </a>

                    <a href="#" class="completa">Veja a Programação completa »</a>
                </div>

                <div class="assuntos-relacionados">
                    <p class="titulo">ASSUNTOS RELACIONADOS:</p>
                    <a href="#" class="noticia">
                        <span class="titulo">TÍTULO DA NOTÍCIA EM DESTAQUE</span>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis, aperiam!
                        <span class="ler-mais">LER MAIS »</span>
                    </a>
                    <a href="#" class="noticia">
                        <span class="titulo">NOVA PORTARIA 5599/63 PARA A PRODUÇÃO DE BRINQUEDOS É APROVADA!</span>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dignissimos blanditiis obcaecati molestias qui. Qui, ab?
                        <span class="ler-mais">LER MAIS »</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
