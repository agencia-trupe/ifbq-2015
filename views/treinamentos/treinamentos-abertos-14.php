    <div class="main treinamentos">
        <div class="title"></div>

        <div class="center">
            <div class="aside">
<?php
$sub = 'treinamentos-abertos';
include 'include/aside.php';
?>
            </div>

            <div class="conteudo">
                <img src="<?=$url?>assets/img/layout/img-treinamentos-abertos.png" alt="">

                <h3>
                    TREINAMENTOS ABERTOS
                    <span class="area">área de atuação: <strong>QUALIDADE</strong></span>
                </h3>

                <div class="treinamento-descricao no-margin">
                    <div class="titulo">Nome do treinamento completo</div>
                    <div class="treinamento-data no-hover">
                        <div class="data">23 <strong>JUNHO</strong> 2015</div>
                        <div class="endereco">
                            <span><strong>São Paulo - SP</strong> | Auditório do Instituto Falcão Bauer</span>
                            <span class="icone">Rua do Endereço Completo, 123 · Bairro da Vila · Cidade, UF</span>
                        </div>
                    </div>
                </div>

                <h4>FORMULÁRIO DE INSCRIÇÃO</h4>
                <div class="inscricao-wrapper">
                    <div class="passos">
                        <div class="passo">
                            <span>CRIAR LOGIN</span>
                            <span class="numero">1</span>
                        </div>
                        <div class="passo">
                            <span>INFORMAR<br>DADOS PESSOAIS</span>
                            <span class="numero">2</span>
                        </div>
                        <div class="passo active">
                            <span>INFORMAR<br>DADOS DE COBRANÇA</span>
                            <span class="numero">3</span>
                        </div>
                        <div class="passo">
                            <span>CONFIRMAR E<br>FINALIZAR INSCRIÇÃO</span>
                            <span class="numero">4</span>
                        </div>
                    </div>

                    <h5>NOVO CADASTRO</h5>
                    <h6>DADOS DO RESPONSÁVEL PELO PAGAMENTO | PESSOA JURÍDICA</h6>
                    <form action="" method="post" class="form-inscricao-padrao">
                        <div class="row">
                            <label for="cnpj">CNPJ</label>
                            <div class="formulario">
                                <input type="text" name="cnpj" id="cnpj">
                            </div>
                        </div>
                        <div class="row">
                            <label for="razaosocial">razão social</label>
                            <div class="formulario">
                                <input type="razaosocial" name="razaosocial" id="razaosocial">
                            </div>
                        </div>
                        <div class="row">
                            <label for="inscricaoestadual">inscrição estadual</label>
                            <div class="formulario">
                                <input type="text" name="inscricaoestadual" id="inscricaoestadual">
                            </div>
                        </div>
                        <div class="row">
                            <label for="cep">CEP</label>
                            <div class="formulario">
                                <input type="text" name="cep" id="cep">
                            </div>
                        </div>
                        <div class="row">
                            <label for="enderecoempresa">endereço empresa</label>
                            <div class="formulario">
                                <input type="text" name="enderecoempresa" id="enderecoempresa">
                            </div>
                        </div>
                        <div class="row">
                            <label for="numero">número</label>
                            <div class="formulario">
                                <input type="text" name="numero" id="numero">
                            </div>
                        </div>
                        <div class="row">
                            <label for="complemento">complemento</label>
                            <div class="formulario">
                                <input type="text" name="complemento" id="complemento">
                            </div>
                        </div>
                        <div class="row">
                            <label for="bairro">bairro</label>
                            <div class="formulario">
                                <input type="text" name="bairro" id="bairro">
                            </div>
                        </div>
                        <div class="row">
                            <label for="cidade">cidade</label>
                            <div class="formulario">
                                <input type="text" name="cidade" id="cidade">
                            </div>
                        </div>
                        <div class="row">
                            <label for="uf">UF</label>
                            <div class="formulario">
                                <input type="text" name="uf" id="uf">
                            </div>
                        </div>
                        <div class="row">
                            <label>endereço de envio NF</label>
                            <div class="formulario">
                                <label>
                                    <input type="radio" name="enderecoenvionf" value="mesmo" class="form-novoendereco-handle" checked>
                                    o mesmo
                                </label>
                                <label>
                                    <input type="radio" name="enderecoenvionf" value="novo" class="form-novoendereco-handle">
                                    cadastrar novo
                                </label>
                            </div>
                        </div>
                        <div class="form-novo-endereco">
                            <div class="row">
                                <label for="nf-cep">CEP</label>
                                <div class="formulario">
                                    <input type="text" name="nf-cep" id="nf-cep">
                                </div>
                            </div>
                            <div class="row">
                                <label for="nf-endereco">endereço</label>
                                <div class="formulario">
                                    <input type="text" name="nf-endereco" id="nf-endereco">
                                </div>
                            </div>
                            <div class="row">
                                <label for="nf-numero">número</label>
                                <div class="formulario">
                                    <input type="text" name="nf-numero" id="nf-numero">
                                </div>
                            </div>
                            <div class="row">
                                <label for="nf-complemento">complemento</label>
                                <div class="formulario">
                                    <input type="text" name="nf-complemento" id="nf-complemento">
                                </div>
                            </div>
                            <div class="row">
                                <label for="nf-bairro">bairro</label>
                                <div class="formulario">
                                    <input type="text" name="nf-bairro" id="nf-bairro">
                                </div>
                            </div>
                            <div class="row">
                                <label for="nf-cidade">cidade</label>
                                <div class="formulario">
                                    <input type="text" name="nf-cidade" id="nf-cidade">
                                </div>
                            </div>
                            <div class="row">
                                <label for="nf-uf">UF</label>
                                <div class="formulario">
                                    <input type="text" name="nf-uf" id="nf-uf">
                                </div>
                            </div>
                        </div>
                        <div class="row row-two-lines">
                            <label for="responsavelnf">responsável<br>recebim. NF / depto</label>
                            <div class="formulario">
                                <input type="text" name="responsavelnf" id="responsavelnf">
                            </div>
                        </div>
                        <div class="row row-two-lines">
                            <label for="emailresponsavelnf">e-mail do responsável<br>pelo recebimento NF</label>
                            <div class="formulario">
                                <input type="email" name="emailresponsavelnf" id="emailresponsavelnf">
                            </div>
                        </div>
                        <div class="row row-two-lines">
                            <label>necessita número de<br>pedido de compra</label>
                            <div class="formulario">
                                <label style="margin-right:46px;">
                                    <input type="radio" name="numeropedido" value="sim">
                                    sim
                                </label>
                                <label>
                                    <input type="radio" name="numeropedido" value="nao">
                                    não
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <label>endereço de cobrança</label>
                            <div class="formulario">
                                <label>
                                    <input type="radio" name="enderecocobranca" value="mesmo" class="form-novoendereco-handle" checked>
                                    o mesmo
                                </label>
                                <label>
                                    <input type="radio" name="enderecocobranca" value="novo" class="form-novoendereco-handle">
                                    cadastrar novo
                                </label>
                            </div>
                        </div>
                        <div class="form-novo-endereco">
                            <div class="row">
                                <label for="cobranca-cep">CEP</label>
                                <div class="formulario">
                                    <input type="text" name="cobranca-cep" id="cobranca-cep">
                                </div>
                            </div>
                            <div class="row">
                                <label for="cobranca-endereco">endereço</label>
                                <div class="formulario">
                                    <input type="text" name="cobranca-endereco" id="cobranca-endereco">
                                </div>
                            </div>
                            <div class="row">
                                <label for="cobranca-numero">número</label>
                                <div class="formulario">
                                    <input type="text" name="cobranca-numero" id="cobranca-numero">
                                </div>
                            </div>
                            <div class="row">
                                <label for="cobranca-complemento">complemento</label>
                                <div class="formulario">
                                    <input type="text" name="cobranca-complemento" id="cobranca-complemento">
                                </div>
                            </div>
                            <div class="row">
                                <label for="cobranca-bairro">bairro</label>
                                <div class="formulario">
                                    <input type="text" name="cobranca-bairro" id="cobranca-bairro">
                                </div>
                            </div>
                            <div class="row">
                                <label for="cobranca-cidade">cidade</label>
                                <div class="formulario">
                                    <input type="text" name="cobranca-cidade" id="cobranca-cidade">
                                </div>
                            </div>
                            <div class="row">
                                <label for="cobranca-uf">UF</label>
                                <div class="formulario">
                                    <input type="text" name="cobranca-uf" id="cobranca-uf">
                                </div>
                            </div>
                        </div>

                        <h6>DADOS COMPLEMENTARES DO RESPONSÁVEL PELAS INSCRIÇÕES</h6>
                        <div class="row">
                            <label for="telefone">telefone de contato</label>
                            <div class="formulario">
                                <input type="text" name="telefone" id="telefone">
                            </div>
                        </div>
                        <div class="row">
                            <label for="funcaoarea">função / área</label>
                            <div class="formulario">
                                <input type="text" name="funcaoarea" id="funcaoarea">
                            </div>
                        </div>

                        <h6>DADOS DE PAGAMENTO</h6>
                        <label class="forma-pagamento">
                            <input type="radio" name="pagamento" value="boleto">
                            boleto
                        </label>
                        <label class="forma-pagamento">
                            <input type="radio" name="pagamento" value="cartao">
                            cartão de crédito
                        </label>
                        <div class="row">
                            <label>bandeira</label>
                            <div class="formulario">
                                <label>
                                    <input type="radio" name="bandeira" value="visa">
                                    VISA
                                </label>
                                <label>
                                    <input type="radio" name="bandeira" value="mastercard">
                                    Mastercard
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <label for="numero-cartao">número do cartão</label>
                            <div class="formulario">
                                <input type="text" name="numero-cartao" id="numero-cartao">
                            </div>
                        </div>
                        <div class="row">
                            <label for="vencimento-cartao">vencimento</label>
                            <div class="formulario">
                                <input type="text" name="vencimento-cartao" id="vencimento-cartao">
                            </div>
                        </div>
                        <div class="row">
                            <label for="codigo-seguranca-cartao">código de segurança</label>
                            <div class="formulario">
                                <input type="text" name="codigo-seguranca-cartao" id="codigo-seguranca-cartao">
                            </div>
                        </div>
                        <div class="row codigo-promocional">
                            <label for="codigo-promocional">CÓDIGO PROMOCIONAL</label>
                            <div class="formulario">
                                <input type="text" name="codigo-promocional" id="codigo-promocional">
                                <p>Se você possui um código promocional insira-o aqui para obter seu desconto.</p>
                            </div>
                        </div>
                        <a href="#" class="form-submit">
                            <span>PROSSEGUIR: PASSO 4: CONFIRMAR E FINALIZAR INSCRIÇÃO</span>
                        </a>
                    </form>
                </div>
            </div>
        </div>
    </div>
