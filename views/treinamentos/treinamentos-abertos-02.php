    <div class="main treinamentos">
        <div class="title"></div>

        <div class="center">
            <div class="aside">
<?php
$sub = 'treinamentos-abertos';
include 'include/aside.php';
?>
            </div>

            <div class="conteudo">
                <img src="<?=$url?>assets/img/layout/img-treinamentos-abertos.png" alt="">

                <h3>TREINAMENTOS ABERTOS</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi explicabo, corrupti. Deserunt minima dolor consectetur ipsa non, doloremque, libero, delectus quos omnis distinctio voluptates. Ex.</p>

                <div class="areas-de-atuacao-selecionado">
                    <h4>SAÚDE E SEGURANÇA</h4>
                    <div class="imagem">
                        <img src="<?=$url?>assets/img/layout/img-areasatuacao-saudeseg.png" alt="">
                    </div>
                    <div class="treinamentos">
                        <a href="#">Nome do treinamento completo</a>
                        <a href="#">Nome do treinamento lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi, ex.</a>
                        <a href="#">Nome do treinamento completo</a>
                        <a href="#">Nome do treinamento lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi, ex.</a>
                        <a href="#">Nome do treinamento completo</a>
                        <a href="#">Nome do treinamento lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi, ex.</a>
                    </div>
                </div>
                <div class="areas-de-atuacao">
                    <a href="#" class="thumb-small">
                        <div class="imagem">
                            <img src="<?=$url?>assets/img/layout/img-areasatuacao-meioambiente.png" alt="">
                        </div>
                        MEIO AMBIENTE
                    </a>
                    <a href="#" class="thumb-small">
                        <div class="imagem">
                            <img src="<?=$url?>assets/img/layout/img-areasatuacao-normastecnicas.png" alt="">
                        </div>
                        NORMAS TÉCNICAS
                    </a>
                    <a href="#" class="thumb-small">
                        <div class="imagem">
                            <img src="<?=$url?>assets/img/layout/img-areasatuacao-qualidade.png" alt="">
                        </div>
                        QUALIDADE
                    </a>
                    <a href="#" class="thumb-small">
                        <div class="imagem">
                            <img src="<?=$url?>assets/img/layout/img-areasatuacao-rh.png" alt="">
                        </div>
                        RH
                    </a>
                    <a href="#" class="thumb-small">
                        <div class="imagem">
                            <img src="<?=$url?>assets/img/layout/img-areasatuacao-sso.png" alt="">
                        </div>
                        SSO
                    </a>
                    <a href="#" class="thumb-small">
                        <div class="imagem">
                            <img src="<?=$url?>assets/img/layout/img-areasatuacao-sustentabilidade.png" alt="">
                        </div>
                        SUSTENTABILIDADE
                    </a>
                </div>
            </div>
        </div>
    </div>
