    <div class="main treinamentos">
        <div class="title"></div>

        <div class="center">
            <div class="aside">
<?php
$sub = 'interesse';
include 'include/aside.php';
?>
            </div>

            <div class="conteudo">
                <img src="<?=$url?>assets/img/layout/img-treinamentos.png" alt="">

                <h3 class="interesse">
                    REGISTRE SEU INTERESSE
                    <span>Cadastre novos temas, outras datas e/ou horários e sua cidade de preferência.</span>
                </h3>

                <form action="" id="form-interesse" method="post">
                    <input type="text" name="nome" id="nome" placeholder="nome">
                    <input type="email" name="email" id="email" placeholder="e-mail">
                    <input type="text" name="telefone" id="telefone" placeholder="telefone">
                    <select name="tema" id="tema">
                        <option value="" style="display:none;" disabled selected>tema [selecione]</option>
                        <option value="lorem">lorem</option>
                        <option value="ipsum">ipsum</option>
                    </select>
                    <select name="cidade" id="cidade">
                        <option value="" style="display:none;" disabled selected>cidade [selecione]</option>
                        <option value="lorem">lorem</option>
                        <option value="ipsum">ipsum</option>
                    </select>
                    <p>preferência de horário:</p>
                    <label>
                        <input type="checkbox" name="horario[]" value="comercial">
                        horário comercial
                    </label>
                    <label>
                        <input type="checkbox" name="horario[]" value="noturno">
                        noturno
                    </label>
                    <label>
                        <input type="checkbox" name="horario[]" value="sabados">
                        sábados
                    </label>
                    <input type="submit" value="ENVIAR">
                </form>
            </div>
        </div>
    </div>
