    <div class="main treinamentos">
        <div class="title"></div>

        <div class="center">
            <div class="aside">
<?php
$sub = 'instrutores';
include 'include/aside.php';
?>
            </div>

            <div class="conteudo">
                <img src="<?=$url?>assets/img/layout/img-treinamentos-instrutores.png" alt="">

                <h3>NOSSOS INSTRUTORES</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum laudantium iusto eligendi quos eos repellendus soluta non explicabo quia dolor.</p>

                <div class="instrutores">
                    <div class="instrutor">
                        <h4>FULANO DE TAL DA SILVA</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Totam nulla, necessitatibus obcaecati quod voluptas officiis. Alias optio dolore eius, id laborum debitis quos, corrupti voluptas sint enim sequi consequuntur illo.</p>
                    </div>
                    <div class="instrutor">
                        <h4>FULANO DE TAL DA SILVA</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Totam nulla, necessitatibus obcaecati quod voluptas officiis. Alias optio dolore eius, id laborum debitis quos, corrupti voluptas sint enim sequi consequuntur illo.</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolor, nesciunt.</p>
                    </div>
                    <div class="instrutor">
                        <h4>FULANO DE TAL DA SILVA</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Totam nulla, necessitatibus obcaecati quod voluptas officiis. Alias optio dolore eius, id laborum debitis quos, corrupti voluptas sint enim sequi consequuntur illo.</p>
                    </div>
                    <div class="instrutor">
                        <h4>FULANO DE TAL DA SILVA</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Totam nulla, necessitatibus obcaecati quod voluptas officiis. Alias optio dolore eius, id laborum debitis quos, corrupti voluptas sint enim sequi consequuntur illo.</p>
                    </div>
                </div>

                <h3>SEJA TAMBÉM UM INSTRUTOR · ENVIE SEU CURRÍCULO</h3>
                <form action="" id="form-instrutor" method="post">
                    <input type="text" name="nome" id="nome" placeholder="nome">
                    <input type="email" name="email" id="email" placeholder="e-mail">
                    <input type="text" name="telefone" id="telefone" placeholder="telefone">
                    <label id="curriculo">
                        <div class="texto">ANEXAR CURRÍCULO</div>
                        <input type="file" name="curriculo">
                    </label>
                    <input type="submit" value="ENVIAR">
                </form>
            </div>
        </div>
    </div>
