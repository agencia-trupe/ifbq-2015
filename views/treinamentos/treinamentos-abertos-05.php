    <div class="main treinamentos">
        <div class="title"></div>

        <div class="center">
            <div class="aside">
<?php
$sub = 'treinamentos-abertos';
include 'include/aside.php';
?>
            </div>

            <div class="conteudo">
                <img src="<?=$url?>assets/img/layout/img-treinamentos-abertos.png" alt="">

                <h3>
                    TREINAMENTOS ABERTOS
                    <span class="area">área de atuação: <strong>QUALIDADE</strong></span>
                </h3>

                <div class="treinamento-descricao no-margin">
                    <div class="titulo">Nome do treinamento completo</div>
                    <div class="treinamento-data no-hover">
                        <div class="data">23 <strong>JUNHO</strong> 2015</div>
                        <div class="endereco">
                            <span><strong>São Paulo - SP</strong> | Auditório do Instituto Falcão Bauer</span>
                            <span class="icone">Rua do Endereço Completo, 123 · Bairro da Vila · Cidade, UF</span>
                        </div>
                    </div>
                </div>

                <h4>FORMULÁRIO DE INSCRIÇÃO</h4>
                <div class="inscricao-wrapper">
                    <a href="#" class="botao-inicio">NOVO CADASTRO</a>
                    <a href="#" class="botao-inicio">JÁ SOU CLIENTE</a>
                </div>
            </div>
        </div>
    </div>
