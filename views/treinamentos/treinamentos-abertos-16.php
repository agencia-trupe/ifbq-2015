    <div class="main treinamentos">
        <div class="title"></div>

        <div class="center">
            <div class="aside">
<?php
$sub = 'treinamentos-abertos';
include 'include/aside.php';
?>
            </div>

            <div class="conteudo">
                <img src="<?=$url?>assets/img/layout/img-treinamentos-abertos.png" alt="">

                <h3>
                    TREINAMENTOS ABERTOS
                    <span class="area">área de atuação: <strong>QUALIDADE</strong></span>
                </h3>

                <div class="treinamento-descricao no-margin">
                    <div class="titulo">Nome do treinamento completo</div>
                    <div class="treinamento-data no-hover">
                        <div class="data">23 <strong>JUNHO</strong> 2015</div>
                        <div class="endereco">
                            <span><strong>São Paulo - SP</strong> | Auditório do Instituto Falcão Bauer</span>
                            <span class="icone">Rua do Endereço Completo, 123 · Bairro da Vila · Cidade, UF</span>
                        </div>
                    </div>
                </div>

                <h4>FORMULÁRIO DE INSCRIÇÃO</h4>
                <div class="inscricao-wrapper">
                    <div class="passos">
                        <div class="passo">
                            <span>CRIAR LOGIN</span>
                            <span class="numero">1</span>
                        </div>
                        <div class="passo">
                            <span>INFORMAR<br>DADOS PESSOAIS</span>
                            <span class="numero">2</span>
                        </div>
                        <div class="passo">
                            <span>INFORMAR<br>DADOS DE COBRANÇA</span>
                            <span class="numero">3</span>
                        </div>
                        <div class="passo active">
                            <span>CONFIRMAR E<br>FINALIZAR INSCRIÇÃO</span>
                            <span class="numero">4</span>
                        </div>
                    </div>

                    <h5>NOVO CADASTRO</h5>
                    <h6>CONFIRMAÇÃO DOS DADOS DO CURSO</h6>
                    <div class="confirmacao-dados">
                        <div class="row">
                            <label>treinamento</label>
                            <div class="texto-wrapper">
                                <p class="texto">Nome do treinamento completo</p>
                            </div>
                        </div>
                        <div class="row">
                            <label>investimento</label>
                            <div class="texto-wrapper">
                                <p class="texto">R$2.000,00</p>
                            </div>
                        </div>
                        <div class="row">
                            <label>data</label>
                            <div class="texto-wrapper">
                                <p class="texto">23 de junho 2015</p>
                            </div>
                        </div>
                        <div class="row">
                            <label>local</label>
                            <div class="texto-wrapper">
                                <p class="texto">São Paulo - SP | Auditório do Instituto Falcão Bauer</p>
                            </div>
                        </div>
                        <div class="row">
                            <label>endereço</label>
                            <div class="texto-wrapper">
                                <p class="texto">Rua do Endereço Completo, 123 · Bairro da Vila · Cidade, UF</p>
                            </div>
                        </div>
                        <div class="row">
                            <label>programa do curso</label>
                            <div class="texto-wrapper">
                                <p class="texto"><a href="#">» visualizar</a></p>
                            </div>
                        </div>
                    </div>

                    <h6>PARTICIPANTES INSCRITOS</h6>
                    <div class="confirmacao-dados">
                        <p class="texto">Nome do Participante | email@empresa.com.br</p>
                        <p class="texto">Nome do Participante | email@empresa.com.br</p>
                        <p class="texto">Nome do Participante | email@empresa.com.br</p>
                    </div>

                    <p class="aviso-destaque">
                        CADA PARTICIPANTE RECEBERÁ UM E-MAIL PARA CRIAR SEU LOGIN INDIVIDUAL.<br>VOCÊ RECEBERÁ UM E-MAIL COM AS INFORMAÇÕES DO GRUPO CADASTRADO E DO PAGAMENTO.
                    </p>

                    <form action="" method="post" class="form-inscricao-padrao">
                        <label>
                            <input type="checkbox" name="termos" value="1">
                            Li e aceito integralmente todos o <strong>TERMOS E CONDIÇÕES COMERCIAIS</strong> para a aquisição do curso supra citado.
                        </label>
                        <a href="#" class="form-submit">
                            <span class="no-arrow">CONFIRMAR E FINALIZAR INSCRIÇÃO</span>
                        </a>
                    </form>
                </div>
            </div>
        </div>
    </div>
