    <div class="main treinamentos">
        <div class="title"></div>

        <div class="center">
            <div class="aside">
<?php
$sub = 'treinamentos-abertos';
include 'include/aside.php';
?>
            </div>

            <div class="conteudo">
                <img src="<?=$url?>assets/img/layout/img-treinamentos-abertos.png" alt="">

                <h3>
                    TREINAMENTOS ABERTOS
                    <span class="area">área de atuação: <strong>QUALIDADE</strong></span>
                </h3>

                <div class="treinamento-descricao no-margin">
                    <div class="titulo">Nome do treinamento completo</div>
                    <div class="treinamento-data no-hover">
                        <div class="data">23 <strong>JUNHO</strong> 2015</div>
                        <div class="endereco">
                            <span><strong>São Paulo - SP</strong> | Auditório do Instituto Falcão Bauer</span>
                            <span class="icone">Rua do Endereço Completo, 123 · Bairro da Vila · Cidade, UF</span>
                        </div>
                    </div>
                </div>

                <h4>FORMULÁRIO DE INSCRIÇÃO</h4>
                <div class="inscricao-wrapper">
                    <div class="passos">
                        <div class="passo active">
                            <span>CRIAR LOGIN</span>
                            <span class="numero">1</span>
                        </div>
                        <div class="passo">
                            <span>INFORMAR<br>DADOS PESSOAIS</span>
                            <span class="numero">2</span>
                        </div>
                        <div class="passo">
                            <span>INFORMAR<br>DADOS DE COBRANÇA</span>
                            <span class="numero">3</span>
                        </div>
                        <div class="passo">
                            <span>CONFIRMAR E<br>FINALIZAR INSCRIÇÃO</span>
                            <span class="numero">4</span>
                        </div>
                    </div>

                    <h5>NOVO CADASTRO</h5>
                    <h6>DADOS DE LOGIN</h6>
                    <form action="" method="post" class="form-inscricao-padrao">
                        <div class="row">
                            <label for="email">e-mail</label>
                            <div class="formulario">
                                <input type="email" name="email" id="email">
                            </div>
                        </div>
                        <div class="row">
                            <label for="senha">senha</label>
                            <div class="formulario">
                                <input type="senha" name="senha" id="senha">
                            </div>
                        </div>
                        <div class="row">
                            <label for="confirmasenha">confirmar senha</label>
                            <div class="formulario">
                                <input type="confirmasenha" name="confirmasenha" id="confirmasenha">
                            </div>
                        </div>
                        <a href="#" class="form-submit">
                            <span>PROSSEGUIR: PASSO 2: INFORMAR DADOS PESSOAIS</span>
                        </a>
                    </form>
                </div>
            </div>
        </div>
    </div>
