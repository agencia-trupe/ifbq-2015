    <div class="main treinamentos">
        <div class="title"></div>

        <div class="center">
            <div class="aside">
<?php
$sub = 'calendario-programacao';
include 'include/aside.php';
?>
            </div>

            <div class="conteudo">
                <img src="<?=$url?>assets/img/layout/img-treinamentos.png" alt="">

                <div class="proximos-treinamentos calendario">
                    <div class="titulo">
                        <p>CALENDÁRIO - PROGRAMAÇÃO</p>
                    </div>
                    <div class="filtro">
                        <form action="" id="form-treinamentos-filtro" method="post">
                            <p>Ordenar por:</p>
                            <label><input type="radio" name="filtro" value="data">data</label>
                            <label><input type="radio" name="filtro" value="cidade">cidade</label>
                            <label><input type="radio" name="filtro" value="atuacao">área de atuação</label>
                            <label><input type="radio" name="filtro" value="treinamento">nome do treinamento</label>
                        </form>
                        <a href="#">BUSCA AVANÇADA</a>
                    </div>

                    <a href="#" class="treinamento-chamada">
                        <div class="data">
                            <span class="dia">02</span>
                            <span class="mes">JUN</span>
                        </div>
                        <div class="titulo">
                            FORMAÇÃO DE AUDITOR INTERNO ISO 9001:2008
                            <span>Qualidade</span>
                        </div>
                        <div class="local">
                            São Paulo - SP
                        </div>
                        <div class="chamada">
                            INSCREVA-SE
                        </div>
                    </a>
                    <a href="#" class="treinamento-chamada">
                        <div class="data">
                            <span class="dia">06</span>
                            <span class="mes">JUL</span>
                        </div>
                        <div class="titulo">
                            FORMAÇÃO DE AUDITOR INTERNO COM NOME MUITO LONGO PARA OCUPAR DUAS LINHAS ISO 9001:2008
                            <span>Saúde e Segurança</span>
                        </div>
                        <div class="local">
                            São Paulo - SP
                        </div>
                        <div class="chamada">
                            INSCREVA-SE
                        </div>
                    </a>
                    <a href="#" class="treinamento-chamada">
                        <div class="data">
                            <span class="dia">02</span>
                            <span class="mes">JUN</span>
                        </div>
                        <div class="titulo">
                            FORMAÇÃO DE AUDITOR INTERNO ISO 9001:2008
                            <span>Qualidade</span>
                        </div>
                        <div class="local">
                            São Paulo - SP
                        </div>
                        <div class="chamada">
                            INSCREVA-SE
                        </div>
                    </a>
                    <a href="#" class="treinamento-chamada">
                        <div class="data">
                            <span class="dia">06</span>
                            <span class="mes">JUL</span>
                        </div>
                        <div class="titulo">
                            FORMAÇÃO DE AUDITOR INTERNO COM NOME MUITO LONGO PARA OCUPAR DUAS LINHAS ISO 9001:2008
                            <span>Saúde e Segurança</span>
                        </div>
                        <div class="local">
                            São Paulo - SP
                        </div>
                        <div class="chamada">
                            INSCREVA-SE
                        </div>
                    </a>
                    <a href="#" class="treinamento-chamada">
                        <div class="data">
                            <span class="dia">02</span>
                            <span class="mes">JUN</span>
                        </div>
                        <div class="titulo">
                            FORMAÇÃO DE AUDITOR INTERNO ISO 9001:2008
                            <span>Qualidade</span>
                        </div>
                        <div class="local">
                            São Paulo - SP
                        </div>
                        <div class="chamada">
                            INSCREVA-SE
                        </div>
                    </a>
                    <a href="#" class="treinamento-chamada">
                        <div class="data">
                            <span class="dia">06</span>
                            <span class="mes">JUL</span>
                        </div>
                        <div class="titulo">
                            FORMAÇÃO DE AUDITOR INTERNO COM NOME MUITO LONGO PARA OCUPAR DUAS LINHAS ISO 9001:2008
                            <span>Saúde e Segurança</span>
                        </div>
                        <div class="local">
                            São Paulo - SP
                        </div>
                        <div class="chamada">
                            INSCREVA-SE
                        </div>
                    </a>

                    <a href="#" class="programacao-pdf">
                        FAÇA O DOWNLOAD DA PROGRAMAÇÃO COMPLETA EM PDF »
                        <span>Documento para<strong>DOWNLOAD</strong></span>
                    </a>
                </div>
            </div>
        </div>
    </div>
