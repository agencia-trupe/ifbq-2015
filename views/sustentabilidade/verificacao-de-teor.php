    <div class="main sustentabilidade">
        <div class="title"></div>

        <div class="center">
            <div class="aside">
                <h2>SUSTENTABILIDADE</h2>

                <h3>SELOS</h3>
                <nav class="nav-selos">
                    <a href="<?=$url?>sustentabilidade">SELO ECOLÓGICO FALCÃO BAUER</a>
                    <a href="#">FALCÃO BAUER ECOLABEL BRASIL</a>
                    <a href="#">SUSTENTABILIDADE CONDOMINIAL</a>
                    <a href="#">CERTIFICAÇÃO DE ACESSIBILIDADE</a>
                    <a href="#">AVALIAÇÃO DO CICLO DE VIDA</a>
                </nav>

                <h3>OUTROS SERVIÇOS</h3>
                <nav class="nav-servicos">
                    <a href="<?=$url?>sustentabilidade/verificacao-de-teor" class="active">VERIFICAÇÃO DE TEOR DE COMPOSTOS ORGÂNICOS VOLÁTEIS</a>
                    <a href="#">VERIFICAÇÃO DE CONTEÚDO RECICLADO</a>
                    <a href="#">VERIFICAÇÃO DE MATERIAL REGIONAL</a>
                </nav>
            </div>

            <div class="conteudo">
                <img src="<?=$url?>assets/img/layout/img-sustentabilidade.png" alt="">

                <h3>VERIFICAÇÃO DE TEOR DE COMPOSTOS ORGÂNICOS VOLÁTEIS</h3>
                <div class="texto">
                    <ul>
                        <li>Validação de que os materiais que compõem o produto final foram extraídos e manufaturados na região.</li>
                        <li>Realizada a partir de inspeção para verificar quais são as matérias-primas que compõem o produto final, o local de extração, o local de processamento e posteriormente emitir um documento com o objetivo de qualificar o produto avaliado para programas de compra de produtos ambientalmente preferíveis e atendimento aos critérios LEED.</li>
                    </ul>

                    <p><strong>Benefícios da Certificação</strong></p>
                    <ul>
                        <li>Demonstrar compromisso com o desenvolvimento local</li>
                        <li>Aumentar a demanda por materiais e produtos regionais</li>
                        <li>Reduzir os impactos provenientes do transporte e distribuição</li>
                        <li>Atender especificações de compra de deerminados clientes</li>
                    </ul>
                </div>

                <div class="documentos">
                    <span class="subtitulo">Documentos para download:</span>
                    <a href="#">Nome do documento para download</a>
                    <a href="#">Nome do documento para download</a>
                </div>
            </div>

            <div class="selos">
                <h4>Todos os Selos:</h4>
                <div>
                    <a href="#">
                        <div style="background-image: url('<?=$url?>assets/img/layout/selo-ecologico.png')"></div>
                        <span>SELO ECOLÓGICO FALCÃO BAUER</span>
                    </a>
                    <a href="#">
                        <div style="background-image: url('<?=$url?>assets/img/layout/selo-ecolabel.png')"></div>
                        <span>FALCÃO BAUER ECOLABEL BRASIL</span>
                    </a>
                    <a href="#">
                        <div style="background-image: url('<?=$url?>assets/img/layout/selo-ecolabel.png')"></div>
                        <span>SUSTENTABILIDADE CONDOMINIAL</span>
                    </a>
                    <a href="#">
                        <div style="background-image: url('<?=$url?>assets/img/layout/selo-ecolabel.png')"></div>
                        <span>CERTIFICAÇÃO DE ACESSIBILIDADE</span>
                    </a>
                    <a href="#">
                        <div style="background-image: url('<?=$url?>assets/img/layout/selo-ecolabel.png')"></div>
                        <span>AVALIAÇÃO DO CICLO DE VIDA</span>
                    </a>
                </div>
            </div>

            <div class="servicos">
                <h4>Outros serviços:</h4>
                <div>
                    <a href="#">
                        <img src="<?=$url?>assets/img/layout/img-selos-compostosorganicos.png">
                        <span>VERIFICAÇÃO DE TEOR DE COMPOSTOS ORGÂNICOS VOLÁTEIS</span>
                    </a>
                    <a href="#">
                        <img src="<?=$url?>assets/img/layout/img-selos-reciclado.png">
                        <span>VERIFICAÇÃO DE CONTEÚDO RECICLADO</span>
                    </a>
                    <a href="#">
                        <img src="<?=$url?>assets/img/layout/img-selos-regional.png">
                        <span>VERIFICAÇÃO DE MATERIAL REGIONAL</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
