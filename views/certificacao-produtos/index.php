    <div class="main certificacao-produtos">
        <div class="title"></div>

        <div class="center">
            <div class="aside">
                <h2>CERTIFICAÇÃO DE PRODUTOS</h2>

                <h3>CERTIFICAÇÃO COMPULSÓRIA</h3>
                <nav class="nav-compulsoria">
                    <a href="#">PNEUS</a>
                    <a href="#" class="has-sub">AUTOMOTIVO</a>
                    <div class="submenu">
                        <a href="<?=$url?>certificacao-produtos/componentes-automotivos">Componentes Automotivos</a>
                        <a href="#">Vidro Laminado e Temperado Automotivo</a>
                        <a href="#">Rodas Automotivas</a>
                        <a href="#">Componentes de Bicicleta</a>
                    </div>
                    <a href="#">COMBUSTÍVEL</a>
                    <a href="#" class="has-sub">CAPACETE</a>
                    <div class="submenu">
                        <a href="#">Lorem ipsum dolor</a>
                        <a href="#">Lorem ipsum dolor</a>
                    </div>
                    <a href="#">SEGURANÇA PESSOAL</a>
                </nav>

                <h3>CERTIFICAÇÃO VOLUNTÁRIA</h3>
                <nav class="nav-voluntaria">
                    <a href="#">PRODUTOS DA CONSTRUÇÃO CIVIL</a>
                    <a href="#">VIDROS</a>
                    <a href="#">ROHS</a>
                    <a href="#">INFORMÁTICA</a>
                    <a href="#">ÁGUA MINERAL</a>
                </nav>
            </div>

            <div class="conteudo">
                <img src="<?=$url?>assets/img/layout/img-certificacaoprodutos.png" alt="">

                <h3>CERTIFICAÇÃO COMPULSÓRIA</h3>
                <p>A certificação compulsória de produtos, regulamentada pelo INMETRO, é necessária para aqueles produtos que apresentam riscos à segurança do consumidor, ao meio ambiente ou quando seu desempenho possa trazer prejuízos econômicos à sociedade. Após o processo de certificação, estes produtos devem ser comercializados com o Selo de Identificação de Conformidade, com logo do INMETRO e do IFBQ.</p>
                <img src="<?=$url?>assets/img/layout/img-certificacaoprodutos2.png" alt="">

                <h3>CERTIFICAÇÃO VOLUNTÁRIA</h3>
                <p>A certificação voluntária é uma decisão exclusiva das empresas fabricantes ou importadoras que desejam ter um diferencial de qualidade e demonstrar credibilidade de seus produtos e serviços. Os produtos certificados voluntariamente são comercializados com o Selo IFBQ, e caso haja um RAC - Regulamento de Avaliação da Conformidade - podem ostentar também o selo do INMETRO.</p>
                <img src="<?=$url?>assets/img/layout/img-certificacaoprodutos3.png" alt="">
            </div>

        </div>
    </div>
