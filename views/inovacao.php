    <div class="main inovacao">
        <div class="title"></div>

        <div class="center">
            <div class="aside">
                <h2>INOVAÇÃO NA CONSTRUÇÃO CIVIL</h2>
            </div>

            <div class="conteudo">
                <img src="<?=$url?>assets/img/layout/img-inovacaoconstrucaocivil.png" alt="">
                <h3>Avaliação Técnica de Desempenho</h3>
                <p>Sistema organizado pelo Ministério das Cidades, o SINAT (Sistema Nacioal de Avaliação Técnica) é uma iniciativa da comunidade técnica nacional da construção civil, que visa uniformizar e avaliar os novos sistemas construtivos e os produtos inovadores disponibilizados no mercado para a obtenção do DATec (Documento Técnico de Avaliação).</p>
            </div>
        </div>
    </div>
