    <div class="main certificacao-sistemas">
        <div class="title"></div>

        <div class="center">
            <div class="aside">
                <h2>CERTIFICAÇÃO DE SISTEMAS DE GESTÃO</h2>
                <p>A certificação voluntária é uma decisão exclusiva das empresas fabricantes ou importadoras que desejam ter um diferencial de qualidade e demonstrar credibilidade de seus produtos e serviços. Os produtos certificados voluntariamente são comercializados com o Selo IFBQ, e caso haja um RAC - Regulamento de Avaliação da Conformidade - podem ostentar também o selo do INMETRO.</p>
                <a href="<?=$url?>certificacao-sistemas/como-obter">Confira os passos para obter uma certificação</a>
            </div>

            <div class="conteudo">
                <img src="<?=$url?>assets/img/layout/img-certificacaosistemas.jpg" alt="">

                <div class="accordion">
                    <a href="#">SASSMAQ - Sistema de Avaliação de Segurança e Saúde</a>
                    <div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nisi nihil esse magnam nulla voluptas dolorem nam accusantium at, nesciunt ratione sequi eligendi consequatur laborum, illo sit a temporibus suscipit, nostrum praesentium quaerat sint repellendus! Harum nesciunt magnam similique, molestias velit voluptatem perferendis sed iusto dignissimos commodi praesentium quis ex eius! Omnis nam officiis praesentium, hic unde expedita amet architecto quos aut odit voluptatem, modi libero quae iste nesciunt sequi adipisci pariatur maxime cum neque excepturi voluptate in ut! Harum, rerum, ab. Eos, sed perferendis ex nesciunt error rem eaque explicabo, quisquam dolore sapiente distinctio optio amet aspernatur consequatur fugit minus.</p>
                    </div>

                    <a href="#">Sistema de Gestão Ambiental - ISO 14001</a>
                    <div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsa velit laboriosam necessitatibus eius, quaerat porro corporis obcaecati debitis eum cumque!</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem possimus repellat inventore soluta iure consequuntur illum, placeat voluptatibus tempora expedita ipsa recusandae harum cupiditate natus rerum nemo! Delectus necessitatibus perferendis excepturi deleniti commodi ut repellat numquam velit. Odit, debitis, earum.</p>
                    </div>

                    <a href="#">Sistema de Gestão da Qualidade - ISO 9001</a>
                    <div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nisi nihil esse magnam nulla voluptas dolorem nam accusantium at, nesciunt ratione sequi eligendi consequatur laborum, illo sit a temporibus suscipit, nostrum praesentium quaerat sint repellendus! Harum nesciunt magnam similique, molestias velit voluptatem perferendis sed iusto dignissimos commodi praesentium quis ex eius! Omnis nam officiis praesentium, hic unde expedita amet architecto quos aut odit voluptatem, modi libero quae iste nesciunt sequi adipisci pariatur maxime cum neque excepturi voluptate in ut! Harum, rerum, ab. Eos, sed perferendis ex nesciunt error rem eaque explicabo, quisquam dolore sapiente distinctio optio amet aspernatur consequatur fugit minus.</p>
                    </div>

                    <a href="#">Sistema de Gestão de Segurança em Turismo de Aventura</a>
                    <div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nisi nihil esse magnam nulla voluptas dolorem nam accusantium at, nesciunt ratione sequi eligendi consequatur laborum, illo sit a temporibus suscipit, nostrum praesentium quaerat sint repellendus! Harum nesciunt magnam similique, molestias velit voluptatem perferendis sed iusto dignissimos commodi praesentium quis ex eius! Omnis nam officiis praesentium, hic unde expedita amet architecto quos aut odit voluptatem, modi libero quae iste nesciunt sequi adipisci pariatur maxime cum neque excepturi voluptate in ut! Harum, rerum, ab. Eos, sed perferendis ex nesciunt error rem eaque explicabo, quisquam dolore sapiente distinctio optio amet aspernatur consequatur fugit minus.</p>
                    </div>
                </div>

                <h3>SOLICITE SUA CERTIFICAÇÃO</h3>
                <form action="" id="form-certificacao">
                    <div class="row">
                        <label for="cnpj">CNPJ</label>
                        <div class="formulario">
                            <input type="text" name="cnpj" id="cnpj">
                        </div>
                    </div>
                    <div class="row">
                        <label for="razaosocial">razão social</label>
                        <div class="formulario">
                            <input type="text" name="razaosocial" id="razaosocial">
                        </div>
                    </div>
                    <div class="row">
                        <label for="contato">contato</label>
                        <div class="formulario">
                            <input type="text" name="contato" id="contato">
                        </div>
                    </div>
                    <div class="row">
                        <label for="telefone">telefone</label>
                        <div class="formulario">
                            <input type="text" name="telefone" id="telefone">
                        </div>
                    </div>
                    <div class="row">
                        <label for="email">e-mail</label>
                        <div class="formulario">
                            <input type="email" name="email" id="email">
                        </div>
                    </div>
                    <div class="row">
                        <label for="site">site</label>
                        <div class="formulario">
                            <input type="text" name="site" id="site">
                        </div>
                    </div>
                    <div class="row">
                        <label for="certificacao">tipo de certificação</label>
                        <div class="formulario">
                            <select name="certificacao" id="certificacao">
                                <option value="" style="display:none;" disabled selected>[selecione]</option>
                                <option value="lorem">lorem</option>
                                <option value="ipsum">ipsum</option>
                            </select>
                        </div>
                    </div>
                    <input type="submit" value="ENVIAR">
                </form>
            </div>
        </div>
    </div>
