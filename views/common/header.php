    <header>
        <div class="header-top">
            <div class="center">
                <h1><a href="<?=$url?>home">Instituto Falcão Bauer</a></h1>

                <form action="<?=$url?>busca" method="post" id="form-busca">
                    <input type="text" name="busca" placeholder="buscar no site" required>
                    <input type="submit" value>
                </form>

                <nav>
                    <a href="<?=$url?>instituto"<?php if($menu == 'instituto') echo 'class="active"' ?>>Instituto Falcão Bauer</a>
                    <a href="<?=$url?>grupo"<?php if($menu == 'grupo') echo 'class="active"' ?>>Grupo Falcão Bauer</a>
                    <a href="<?=$url?>noticias"<?php if($menu == 'noticias') echo 'class="active"' ?>>Notícias</a>
                    <a href="<?=$url?>contato"<?php if($menu == 'contato') echo 'class="active"' ?>>Contato</a>
                </nav>
            </div>
        </div>

        <div class="header-nav center">
            <nav>
                <a href="<?=$url?>treinamentos"<?php if($menu == 'treinamentos') echo 'class="active"' ?>>
                    Treinamentos <span>Cursos</span>
                </a>
                <a href="<?=$url?>certificacao-produtos"<?php if($menu == 'certificacao-produtos') echo 'class="active"' ?>>
                    Certificação <span>Produtos</span>
                </a>
                <a href="<?=$url?>sustentabilidade"<?php if($menu == 'sustentabilidade') echo 'class="active"' ?>>
                    Sustentabilidade <span>Selos e Serviços</span>
                </a>
                <a href="<?=$url?>certificacao-sistemas"<?php if($menu == 'certificacao-sistemas') echo 'class="active"' ?>>
                    Certificação <span>Sistemas de Gestão</span>
                </a>
                <a href="<?=$url?>inovacao"<?php if($menu == 'inovacao') echo 'class="active"' ?>>
                    Inovação <span>Na Construção Civil</span>
                </a>
            </nav>
        </div>

        <div id="nav-mobile">
            <nav>
                <a href="<?=$url?>instituto"<?php if($menu == 'instituto') echo 'class="active"' ?>>Instituto Falcão Bauer</a>
                <a href="<?=$url?>grupo"<?php if($menu == 'grupo') echo 'class="active"' ?>>Grupo Falcão Bauer</a>
                <a href="<?=$url?>noticias"<?php if($menu == 'noticias') echo 'class="active"' ?>>Notícias</a>
                <a href="<?=$url?>contato"<?php if($menu == 'contato') echo 'class="active"' ?>>Contato</a>
                <a href="<?=$url?>treinamentos"<?php if($menu == 'treinamentos') echo 'class="active"' ?>>
                    Treinamentos <span>Cursos</span>
                </a>
                <a href="<?=$url?>certificacao-produtos"<?php if($menu == 'certificacao-produtos') echo 'class="active"' ?>>
                    Certificação <span>Produtos</span>
                </a>
                <a href="<?=$url?>sustentabilidade"<?php if($menu == 'sustentabilidade') echo 'class="active"' ?>>
                    Sustentabilidade <span>Selos e Serviços</span>
                </a>
                <a href="<?=$url?>certificacao-sistemas"<?php if($menu == 'certificacao-sistemas') echo 'class="active"' ?>>
                    Certificação <span>Sistemas de Gestão</span>
                </a>
                <a href="<?=$url?>inovacao"<?php if($menu == 'inovacao') echo 'class="active"' ?>>
                    Inovação <span>Na Construção Civil</span>
                </a>
            </nav>
        </div>
        <button id="mobile-toggle" type="button" role="button">
            <span class="lines"></span>
        </button>
    </header>

