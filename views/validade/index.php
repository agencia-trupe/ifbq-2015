    <div class="main validade">
        <div class="title"></div>

        <div class="center">
            <div class="aside">
                <h2>CONSULTA · VALIDADE DE CERTIFICADOS</h2>
                <img src="<?=$url?>assets/img/layout/certificado.jpg" alt="">
            </div>

            <div class="conteudo">
                <h3>CONSULTE</h3>

                <form action="<?=$url?>validade/certificado-valido" method="post" id="form-validade">
                    <div class="row">
                        <div class="passo passo-1"></div>
                        <div class="formulario">
                            <label>SELECIONE A NORMA:</label>
                            <div class="radios">
                                <label>
                                    <input type="radio" name="norma" value="ISO/IEC 20000:2005">
                                    <span>ISO/IEC 20000:2005</span>
                                </label>
                                <label>
                                    <input type="radio" name="norma" value="ISO/IEC 20000:2011">
                                    <span>ISO/IEC 20000:2011</span>
                                </label>
                                <label>
                                    <input type="radio" name="norma" value="ISO/IEC 27001:2006">
                                    <span>IEC 27001:2006</span>
                                </label>
                                <label>
                                    <input type="radio" name="norma" value="ISO/TS 16949:2002">
                                    <span>ISO/TS 16949:2002</span>
                                </label>
                                <label>
                                    <input type="radio" name="norma" value="ISO/TS 16949:2009">
                                    <span>ISO/TS 16949:2009</span>
                                </label>
                                <label>
                                    <input type="radio" name="norma" value="NBR 15100:2004">
                                    <span>NBR 15100:2004</span>
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="passo passo-2"></div>
                        <div class="formulario">
                            <label for="empresa">INFORME O NOME DA EMPRESA:</label>
                            <input type="text" name="empresa" id="empresa">
                        </div>
                    </div>

                    <div class="row">
                        <div class="passo passo-3"></div>
                        <div class="formulario">
                            <label for="certificado">INFORME O NÚMERO DO CERTIFICADO:</label>
                            <input type="text" name="certificado" id="certificado">
                        </div>
                    </div>

                    <input type="submit" value="CONSULTAR">
                </form>
            </div>
        </div>
    </div>
